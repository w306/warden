""" Data preprocessing module

This file contains 2 utilitary procedures for the project: video frame extraction and 
dataframe creation, which consolidates the resulting frame names into 2 separate
datasets.

The video frames are mandatory because machine learning models cannot make use of raw input
videos, they must be preprocessed into static "chunks". These pieces are, then, fed to the 
autoencoder for feature learning.

"""
import os
import shutil
import pandas as pd
import cv2
import math
import re

# Preprocessing the dataset to get all video frames
def extract_frames():
    path = "build/dataset/cosmo_dataset/"
    extract_path = "build/dataset/"
    dataframe = pd.read_csv(os.path.join(path + "names.csv"))
    train_videos = dataframe[:22]
    test_videos = dataframe[22:34]

    try:
        os.makedirs(path)
        os.makedirs(extract_path + "train/legit")
        os.makedirs(extract_path + "train/cheater")
        os.makedirs(extract_path + "test/legit")
        os.makedirs(extract_path + "test/cheater")

    except OSError:
        pass

    # Extracting frames from each training video
    for video in train_videos["name"]:
        count = 0
        videoFile = video
        cap = cv2.VideoCapture(os.path.join(path + videoFile + ".mp4"))   # capturing the videos from the dataset
        frameRate = cap.get(5) # framerate

        while(cap.isOpened()):
            frameId = cap.get(1) # current frame number
            ret, frame = cap.read()

            if (ret != True):
                break

            if (frameId % math.floor(frameRate) == 0):
                # Saving the frames in a new folder named "train"
                # If the video is of a cheater, saves on a subfolder named "cheater"
                # Saves on a subfolder named "legit" otherwise
                if re.search("(01)", videoFile):
                    filename = os.path.join(extract_path + "train/cheater/" + videoFile + "_frame%d.png" % count)

                else:
                    filename = os.path.join(extract_path + "train/legit/" + videoFile + "_frame%d.png" % count)
                
                count += 1

                if not cv2.imwrite(filename, frame):
                    raise Exception("Could not write image")

        cap.release() 
    
    # Extracting frames from each testing video
    for video in test_videos["name"]:
        count = 0
        videoFile = video
        cap = cv2.VideoCapture(os.path.join(path + videoFile + ".mp4"))   # capturing the videos from the dataset
        frameRate = cap.get(5) # framerate

        while(cap.isOpened()):
            frameId = cap.get(1) # current frame number
            ret, frame = cap.read()

            if (ret != True):
                break

            if (frameId % math.floor(frameRate) == 0):
                # Saving the frames in a new folder named "train"
                # If the video is of a cheater, saves on a subfolder named "cheater"
                # Saves on a subfolder named "legit" otherwise
                if re.search("(01)", videoFile):
                    filename = os.path.join(extract_path + "test/cheater/" + videoFile + "_frame%d.png" % count)

                else:
                    filename = os.path.join(extract_path + "test/legit/" + videoFile + "_frame%d.png" % count)
                
                count += 1

                if not cv2.imwrite(filename, frame):
                    raise Exception("Could not write image")

        cap.release()