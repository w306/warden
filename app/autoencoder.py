import torch
import torch.utils.data
import torch.nn.functional as F
import pytorch_lightning as pl
import numpy as np
from argparse import Namespace
from torch import nn
from torchvision import transforms, datasets
from torch.utils.data import DataLoader
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.callbacks.early_stopping import EarlyStopping

# Deep autoencoder, offers a better performance than a shallow autoencoder
class Encoder(nn.Module):
    def __init__(self):
        super().__init__()
        # The encoder: encoding the output and extracting features
        self.encoder = nn.Sequential(
            nn.Linear(256 * 256 * 3, 64),
            nn.ReLU(),
            nn.Linear(64, 128),
            nn.Dropout(0.1),
            nn.ReLU(),
            nn.Linear(128, 64)
        )
    
    def forward(self, x):
        return self.encoder(x)


class Decoder(nn.Module):
    def __init__(self):
        super().__init__()
        # The encoder: encoding the output and extracting features
        self.decoder = nn.Sequential(
            nn.Linear(64, 128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.Dropout(0.1),
            nn.ReLU(),
            nn.Linear(64, 256 * 256 * 3)
        )
    
    def forward(self, x):
        return self.decoder(x)

class Autoencoder(pl.LightningModule):
    def __init__(self):
        super().__init__()
        # The encoder: encoding the output and extracting features
        self.encoder = Encoder()
        # The decoder: rebuilding the input based on learned features, inverse of encoder
        self.decoder = Decoder()

    # Forward pass, only uses the encoder
    def forward(self, x):
        z = self.encoder(x)
        x_hat = self.decoder(z)

        return x_hat

    # Training step performed by the model
    # Batches are used for better training performance
    def training_step(self, batch, batch_idx):
        x, y = batch
        x = x.view(x.size(0), -1) # reshaping the input to an appropriate shape
        x_hat = self.forward(x)
        loss = F.mse_loss(x_hat, x) # Mean Squared Error, may be replaced by a better loss function if necessary
        # Logging to TensorBoard by default
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True)

        return loss

    # Validation step performed by the model
    # Same as the training step, only with no loss returned
    def validation_step(self, batch, batch_idx):
        x, y = batch
        x = x.view(x.size(0), -1)
        x_hat = self.forward(x)
        loss = F.mse_loss(x_hat, x)
        self.log("valid_loss", loss, on_step=True)

    # Test step performed by the model
    # Same as the training step, only with no loss returned
    def test_step(self, batch, batch_idx):
        x, y = batch
        x = x.view(x.size(0), -1)
        x_hat = self.forward(x)
        loss = F.mse_loss(x_hat, x)
        self.log("test_loss", loss, on_step=True, on_epoch=True, prog_bar=True)

    # Defining an optimizer for the model
    # As of now, Adam seems to work very well 
    # The learning rate of 1e-3 seems to be a "magical constant" for many neural net models
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        
        return optimizer

# The CoSMo (Counter Strike Matches cOllection) data module
# The data module will apply the necessary transformations to the dataset
class CSDataModule(pl.LightningDataModule):
    def __init__(self, data_params):
        super().__init__()
        self.train_data_dir = data_params.train_dir
        self.test_data_dir = data_params.val_dir

    # Training dataloader that uses the training folder
    # To do: normalize the images (transforms.Normalize). Mean and std are required (must be calculated)
    def train_dataloader(self):
        # Applying a sequence of transformations to the image
        train_transform = transforms.Compose([
            transforms.Resize((256, 256)),
            transforms.ToTensor(),     
        ])

        dataset = datasets.ImageFolder(self.train_data_dir, transform=train_transform) 

        return DataLoader(dataset, batch_size=64, shuffle=True, num_workers=16)

    # Validation dataloader that uses the testing data folder
    def val_dataloader(self):
        test_transform = transforms.Compose([
            transforms.Resize((256, 256)),
            transforms.ToTensor()     
        ])

        dataset = datasets.ImageFolder(self.test_data_dir, transform=test_transform) 

        return DataLoader(dataset, batch_size=64, num_workers=16)

# Main function to train the autoencoder
# Note: the namespace is not needed, but is a good way to consolidate parameters
def train():
    data_params = Namespace(
        train_dir="build/dataset/train/",
        val_dir="build/dataset/test/"
    )

    checkpoint_callback = ModelCheckpoint(
        monitor="valid_loss",
        dirpath="build/trained_models/",
        filename="autoencoder-csgo",
        save_top_k=3,
        mode="min"
)

    model = Autoencoder()
    data_module = CSDataModule(data_params)
    trainer = pl.Trainer(
        gpus=1,
        callbacks=[checkpoint_callback], # keeping an eye on the validation loss for early stopping
        auto_lr_find=True, # parameter to automatically find the best learning rate
        max_epochs=20 # max number of epochs
    )  
    
    # Fitting the model
    trainer.fit(model, data_module)

if __name__ == "__main__":
    #extract_frames()
    train()