""" Warden system - ensemble model 

The ensemble model is a stacking classifier constituted by the following algorithms:

- Random forest classifier;
- K-nearest neighbors classifier;
- C-support vector classifier.

All models are hyperparameter-tuned and have standardized inputs.

The ensemble is the penultimate prediction layer of the Warden anti-cheat system, and 
will take, as input, the autoencoder's encoder. Contained here is also a logistic 
regression model, the system's final estimator which takes the ensemble's stacked predictions
as its input and outputs a final classification.

"""
import torch
from joblib import dump
from autoencoder import Autoencoder
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, StackingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score #, precision_score, confusion_matrix
from torchvision import transforms, datasets
from torch.utils.data import DataLoader

# Loading the images
def load_images():
    test_path = "build/dataset/test/"

    test_transform = transforms.Compose([
            transforms.Resize((256, 256)),
            transforms.ToTensor()     
    ])

    test_imgs = datasets.ImageFolder(test_path, transform=test_transform)

    dataset = DataLoader(test_imgs, batch_size=64, shuffle=True)
    
    images = next(iter(dataset))[0]
    labels = next(iter(dataset))[1]

    return images.numpy(), labels.numpy()

# Loading the encoder and encoding the input and output data
def encode_data(X, y):
    model = Autoencoder.load_from_checkpoint("build/trained_models/autoencoder-csgo.ckpt", strict=False)

    model.eval()

    # X = torch.from_numpy(X).view(X.size(0), -1).numpy()

    X = torch.Tensor(X)
    X = X.view(X.size(0), -1)

    X_encoded = model.encoder(X).detach().numpy()

    X_train, X_test, y_train, y_test = train_test_split(X_encoded, y, test_size=0.25, random_state=0)

    return X_train, X_test, y_train, y_test

# Creation of the ensemble model, following 2 tactics: traditional train, test and split and
# stratified k-fold. Whichever approach is the most efficient will constitute the final model. 
def create_ensemble(X_train, y_train):
    # Creating and applying a pipeline to each model
    rfc_pipeline = make_pipeline(StandardScaler(), RandomForestClassifier(random_state=0, n_estimators=1000))
    knn_pipeline = make_pipeline(StandardScaler(), KNeighborsClassifier())
    svc_pipeline = make_pipeline(StandardScaler(), SVC())

    # Adding all models to the ensemble model
    estimators = [
        ("rfc", rfc_pipeline),
        ("knn", knn_pipeline),
        ("svc", svc_pipeline)
    ]

    # Fitting the model
    clf_tts = StackingClassifier(estimators=estimators, final_estimator=LogisticRegression())
    fit_clf_tts = clf_tts.fit(X_train, y_train)

    return fit_clf_tts

# Main runner function
if __name__ == "__main__":
    X, y = load_images()
    X_train, X_test, y_train, y_test = encode_data(X, y)
    tts_model = create_ensemble(X_train, y_train)

    print(tts_model.get_params().keys())

    tts_prediction = tts_model.predict(X_test)

    print(f"Ensemble accuracy score: {accuracy_score(y_test, tts_prediction) * 100}%")

    # Saving the model
    dump(tts_model, "build/trained_models/ensemble-csgo.joblib")