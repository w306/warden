import streamlit as st
import cv2
import os
import shutil
import extract
from autoencoder import Autoencoder
from torchvision import transforms, datasets
from torch.utils.data import DataLoader
from torch import torch
from glob import glob
import numpy as np
from PIL import Image
from joblib import load
from scipy import stats

torch.manual_seed(1)

def load_video(video_file):
    video_file = open(video_file, 'rb')
    return video_file.read()

def validate_file_duration(path):
    cap = cv2.VideoCapture(path)
    fps = cap.get(cv2.CAP_PROP_FPS)      # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    return frame_count/fps

def main():
    st.title("Warden Anti-cheat")

    video_file = st.file_uploader("upload a video", type=["mp4"])
    try:
        os.makedirs("temp")
    except OSError:
        pass

    if video_file is not None:
        video = video_file.name
        path = 'temp/' + video 

        with open(path, mode='wb') as f:
            f.write(video_file.read())

        is_video_valid(path)
        # load video into the UI
        loaded_video = load_video('temp/' + video_file.name)
        st.video(loaded_video)

        extract.extract_frames(video_file.name)
        model = Autoencoder.load_from_checkpoint("../build/trained_models/autoencoder-csgo.ckpt", strict=False)
        model.eval()

        images = load_images()
        X = torch.Tensor(images)
        X = X.view(X.size(0), -1)
        X_encoded = model.encoder(X).detach().numpy()
        ensemble = load("../build/trained_models/ensemble-csgo.joblib")
        model_prediction = ensemble.predict(X_encoded)
        count = np.count_nonzero(model_prediction == 1)

        print(model_prediction)

        if count >= len(images)/3:
            output = "CHEATER"
        else:
            output = "LEGIT" 

        st.write(output)

    
    shutil.rmtree("temp/", ignore_errors=True)

 #Loading the images
def load_images():
    path = "temp/frames/"

    imgs = []
    
    test_transform = transforms.Compose([
            transforms.Resize((256, 256)),
            transforms.ToTensor()
    ])

    for img in glob(path + "*.png"):
      image = Image.open(img)
      image = test_transform(image)
      imgs.append(np.array(image))

    return np.array(imgs) 


def is_video_valid(path):
    video_duration = validate_file_duration(path)
    if(video_duration > 99999999999):
        shutil.rmtree("temp/")
        st.error("invalid size")
        return

if __name__ == '__main__':
    main()