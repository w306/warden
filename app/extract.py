""" Data processing module for the Gradio page

This simple script extracts the frames from the input video and saves them 
in a new folder.
"""
import os
import cv2
import math

# Processing the video to get all video frames
def extract_frames(video_name):
    video_path = "temp/"
    extract_path = "temp/frames/"

    try:
        os.makedirs(extract_path)

    except OSError:
        pass

    # Extracting frames from the input video
    cap = cv2.VideoCapture(os.path.join(video_path + video_name)) 
    frameRate = cap.get(5) # framerate
    count = 0
    while(cap.isOpened()):
        frameId = cap.get(1) # current frame number
        ret, frame = cap.read()

        if (ret != True):
            break

        if (frameId % math.floor(frameRate) == 0):
            # Saving the frames in a new folder named "frames"
            filename = os.path.join(extract_path + video_name + "_frame%d.png" % count)

            count += 1

            if not cv2.imwrite(filename, frame):
                raise Exception("Could not write image")

    cap.release() 