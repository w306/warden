## The Warden Project
> Automated cheat detection through machine learning algorithms

***

## About
This repository contains the overall structure of a complex system that's currently being developed by [Rodrigo Pereira](https://www.gitlab.com/peroz/) and [Rafael Reis](https://www.gitlab.com/4KXP), creators of the [Counter Strike Matches cOllection (CoSMo)](https://archive.org/details/cosmo_dataset) dataset. It's part of our final project at PUC Minas' Artificial Intelligence and Machine Learning graduate course.

Warden is a multi-layered anti-cheat system that is built upon 3 foundations:

* Video processing and analysis;
* Feature learning through unsupervised learning;
* Supervised learning and prediction.

When it comes to the overall machine learning architecture, the system is a classifier that works through steps; said steps work as layers of machine learning models and data preprocessing, which are illustrated in the image below:

<div align="center">
    <img src="src/img/ml-architecture.png">
    <small><b>Source:</b> the authors</small>
</div>

## Roadmap
The Warden Project is in its later stages of development. The overall development plan for the system is:

- [x] Add a folder meant only for model training;
- [x] Develop the project's overall infrastructure;
- [x] Include the autoencoder in the project's main folder;
- [x] Modify the ensemble model and test it;
- [x] Improve the autoencoder through hyperparameter tuning;
- [x] Include the ensemble model in the project's main folder;
- [x] Improve the ensemble model through hyperparameter tuning; 
- [X] Add a landing page that allows users to submit videos;
- [ ] Use GitLab's CI/CD to build a pipeline.

## Installation
The `requirements.txt` file covers all of Warden's used libraries. As such, a simple `pip install -r requirements.txt` will install all of the project's dependencies.

## Acknowledgments
The idea behind this project was largely inspired by [Alexandre Philbert's paper](https://www.imedpub.com/articles/detecting-cheating-in-computer-games-using-data-mining-methods.pdf) on the American Journal of Computer Science and Information Technology, which details how data mining and machine learning methods can aid in the detection of cheats in the games Counter Strike, Counter Strike: Global Offensive and Counter Strike: Source.
